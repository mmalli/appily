# Appily

## General Information

Appily is a multiplatform senior project which aims to ease household chore distribution by the help of a smart algorithm. Developers:

1.   Merve Mallı, mervemalli97@gmail.com
2.   Doğaç Oğuz, dogacogz@gmail.com
3.   Rıza Can Sevinç, rcsevinc@gmail.com
